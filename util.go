/*
utility functions for easier error handling and easier command piping
*/
package util

import (
   "os"
   "os/exec"
   "io"
   "io/ioutil"
   "regexp"
   "bufio"
   "bytes"
)

// wrapper for ioutil TempFile function
// panick on TempFile() error
func MustTempFile(dir, prefix string) (file *os.File) { 
   var err error
   if file, err = ioutil.TempFile(dir, prefix); err != nil {
      panic("cannot create temp file: " + err.Error())
   }
   return
}

// wrapper for os MkdirAll function
// panick on MkdirAll error
func MustMkdirAll(path string, perm os.FileMode) {
   var e error
   if e = os.MkdirAll(path, perm); e != nil {
      panic("cannot create dir: " + e.Error())
   }
}

// read from r, which is output from a program's piped output
// output buf is only ready after sem output data
//
// eg
// set up command
// ReadPipe(cmd.stdoutpipe)
// run cmd
// <-sem
// buf contain stdout data from cmd
func ReadPipe(r io.Reader) (buf *bytes.Buffer, sem chan int) {
   buf = new(bytes.Buffer)
   sem = make(chan int)
   var scn = bufio.NewScanner(r)
   go func() {
      for scn.Scan() {
         buf.WriteString(scn.Text() + "\n")
      }
      sem <- 1
   }()
   return
}

// wrapper for os Open function
// take care of error by panicking if Open fail
func MustOpen(name string) (file *os.File) {
   var e error
   if file, e = os.Open(name); e != nil {
      panic("cannot open file: " + name + ", error: " + e.Error())
   }
   return
}

// wrapper for os Close function
// take care of error by panicking if Close fail
func MustClose(file *os.File) {
   var e error
   if e = file.Close(); e != nil {
      panic("cannot close file: " + e.Error())
   }
}

// wrapper for os Write function
// panick on Write error
func MustWrite(file *os.File, buf []byte) {
   var e error
   if _, e = file.Write(buf); e != nil {
      panic("cannot write file: " + e.Error())
   }
}

// wrapper for os/exec Cmd.StdoutPipe
// take care of error by panicking if Cmd.StdoutPipe fail
func MustStdoutPipe(cmd *exec.Cmd) (rd io.ReadCloser) {
   var e error
   if rd, e = cmd.StdoutPipe(); e != nil {
      panic("cannot get stdoutpipe for cmd: " + cmd.Path + ", error: " + e.Error())
   }
   return
}

// wrapper for os/exec Cmd.Start
// take care of error by panicking if Cmd.Start fail
func MustStart(cmd *exec.Cmd) {
   var e error
   if e = cmd.Start(); e != nil {
      panic("cannot start cmd: " + e.Error())
   }
}

// wrapper for os/exec Cmd.Wait
// take care of error by panicking if Cmd.Wait fail
func MustWait(cmd *exec.Cmd) {
   var e error
   if e = cmd.Wait(); e != nil {
      panic("cannot wait cmd: " + e.Error())
   }
}

// wrapper for os/exec Cmd.Run
// take care of error by panicking if Cmd.Run fail
func MustRun(cmd *exec.Cmd) {
   MustStart(cmd)
   MustWait(cmd)
}

// wrapper for os RemoveAll
// panic on RemoveAll error
func MustRemoveAll(path string) {
   var e error
   if e = os.RemoveAll(path); e != nil {
      panic("cannot remove all: " + e.Error())
   }
}

// parse program execution with first argument being program name,
// and second argument being text file
// each line in text file is colon seperated (no space allowed)
// result is map[string]string data structure
// with key = lhs of : in each line in file
// and value = rhs of : in each line in file
// any error cause panic
//
// eg
// input file contain
// `verbose:true`
//
// output map
// map["verbose"]="true"
func MustParseInput() (args map[string]string) {
   if len(os.Args) != 2 {
      panic("wrong number of input")
   }
   var fid = MustOpen(os.Args[1])
   defer MustClose(fid)

   var scn = bufio.NewScanner(fid)
   var regex = regexp.MustCompile(`:`)
   var match []string
   args = map[string]string{}
   for scn.Scan() {
      match = regex.Split(scn.Text(), 2)
      args[match[0]] = match[1]
   }
   return
}

// create piped commands so they can be piped together and executed
type PipeCmd struct {
   Cmds []*exec.Cmd
}

// created piped commands from cmdsIp
// first command in pipe is cmdsIp[0]
// last command in pipe is last item in cmdsIp
// any error cause panic
func MustNewPipeCmd(cmdsIp []*exec.Cmd) (pipe PipeCmd) {
   if len(cmdsIp) < 2 {
      panic("len(cmdsIp) < 2")
   }
   pipe = PipeCmd{
      Cmds: cmdsIp,
   }
   for i := len(cmdsIp)-1; i >= 1; i = i-1 {
      pipe.Cmds[i].Stdin = MustStdoutPipe(pipe.Cmds[i-1])
   }
   return
}

// run piped command in foreground (calling process is blocked until all piped commands finish)
// if in byte slice is not nil, in byte slice is piped to first cmd
// otherwise, nothing is piped to first cmd
// out byte slice is stdout from last cmd in pipe
// any error cause panic except for cmd Wait() error, which is ignored
// reason for ignoring is Wait() can cause trivial error
//
// eg
// piped commands are `echo "hello sun" | grep -o 'hello'`
//
// using in and output byte slice
// in []byte | cat | sort | out []byte
//
func (pipe *PipeCmd) Run(in []byte) (out []byte) {
   var e error
   var w io.WriteCloser
   var buf, sem = ReadPipe(MustStdoutPipe(pipe.Cmds[len(pipe.Cmds)-1]))
   if in == nil {
      // no input to piped cmd
   } else {
      // input need to be piped to first cmd
      if w, e = pipe.Cmds[0].StdinPipe(); e != nil {
         panic("cannot get cmd[0] stdinpipe: " + e.Error())
      }
   }

   for i := len(pipe.Cmds)-1; i >= 0; i = i-1 {
      MustStart(pipe.Cmds[i])
   }
   if w != nil {
      if _, e = w.Write(in); e != nil {
         panic("cannot write: " + e.Error())
      }
      if e = w.Close(); e != nil {
         panic("cannot close: " + e.Error())
      }
   }
   for i := 0; i <= len(pipe.Cmds)-1; i = i+1 {
      pipe.Cmds[i].Wait()
   }
   <-sem
   return buf.Bytes()
}
